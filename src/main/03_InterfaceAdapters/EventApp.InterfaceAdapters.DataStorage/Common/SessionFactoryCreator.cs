﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Driver.MySqlConnector;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Reflection;
using System.Threading;

namespace EventApp.InterfaceAdapters
{
    public static class SessionFactoryCreator
    {
        public static ISessionFactory Create()
        {
            return Fluently
                .Configure()
                .Database(
                    MySQLConfiguration.Standard
                        .ShowSql()
                        .Driver<MySqlConnectorDriver>()
                        .ConnectionString(Environment.GetEnvironmentVariable("DATABASE_CONNECTION_STRING"))
                        //"server=localhost;port=3306;uid=root;password=KahN##13531;database=eventapp"
                )
                .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
                .ExposeConfiguration(config =>
                {
                    // Не обновлять схему
                    //new SchemaUpdate(config).Execute(true, true);

                    // Пересоздавать схему
                    new SchemaExport(config).Create(true, true);
                })
                .BuildSessionFactory();
        }

        public static ISession OpenSessionWithRetries()
        {
            ISession result = null;
            while (true)
            {
                try
                {
                    result = Create().OpenSession();
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.InnerException.Message);
                    Thread.Sleep(500);
                }
            }

            return result;
        }
    }
}
