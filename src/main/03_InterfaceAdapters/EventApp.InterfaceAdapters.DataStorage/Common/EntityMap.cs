﻿using EventApp.Models;
using FluentNHibernate.Mapping;

namespace EventApp.InterfaceAdapters.DataStorage
{
    /// <summary>
    /// Сопоставление полей DTO и полей сущности.
    /// </summary>
    public class EntityMap: ClassMap<Entity>
    {
        public EntityMap()
        {
            Id(x => x.Id).GeneratedBy.GuidComb();

            UseUnionSubclassForInheritanceMapping();
        }
    }
}
