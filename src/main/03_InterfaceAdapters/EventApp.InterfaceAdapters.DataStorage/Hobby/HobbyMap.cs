﻿using EventApp.Models;
using FluentNHibernate.Mapping;

namespace EventApp.InterfaceAdapters.DataStorage
{
    /// <summary>
    /// Сопоставление полей DTO и полей сущности хобби.
    /// </summary>
    public class HobbyMap: SubclassMap<Hobby>
    {
        public HobbyMap()
        {
            Map(x => x.Name).Not.Nullable();
        }
    }
}
