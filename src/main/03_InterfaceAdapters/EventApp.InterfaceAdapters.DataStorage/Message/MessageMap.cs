﻿using EventApp.Models;
using FluentNHibernate.Mapping;

namespace EventApp.InterfaceAdapters.DataStorage
{
    /// <summary>
    /// Сопоставление полей DTO и полей сущности сообщения.
    /// </summary>
    public class MessageMap : SubclassMap<Message>
    {
        public MessageMap()
        {
            References(x => x.Sender).Not.Nullable();
            Map(x => x.Text).Not.Nullable();
            Map(x => x.Created).Not.Nullable();
            References(x => x.Campaign).Not.Nullable();
        }
    }
}
